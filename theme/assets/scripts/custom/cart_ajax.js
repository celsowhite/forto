(function($) {

	$(document).ready(function() {
        
        "use strict";
        
        /*-----------------------
        Get Page Name
        -----------------------*/
        
        function getPageName() {
            
            const body = document.querySelector('body');
        
            const pageName = body.dataset.pageName;
            
            return pageName;

        }

        /*-----------------------
        DCM/FB - Track Add To Cart
        -----------------------*/

        function trackAddToCart(variantId, purchaseType) {

            const trackingPurchaseType = purchaseType === 'onetime' ? 'One Time' : 'Subscribe';

            const hasAccount = getTrackingInfo().customerId ? 'true' : 'false';

            gtag('event', 'conversion', {
                'allow_custom_scripts': true,
                'u1': getTrackingInfo().googleAnalyticsId + '|' + getTrackingInfo().customerId,
                'u3': getTrackingInfo().pageUrl,
                'u5': variantId,
                'u16': trackingPurchaseType,
                'u17': hasAccount,
                'send_to': 'DC-9037312/forto00/forto00+standard'
            });

            fbq('track', 'AddToCart', {
                custID: getTrackingInfo().googleAnalyticsId + ', ' + getTrackingInfo().customerId,
                content_ids: variantId,
                content_type: 'product',
                subscribe: trackingPurchaseType,
                account: hasAccount
            });

        }
        
        /*-----------------------
        Update Cart Icon Count
        ---
        Visually show the cart item count in the header icon.
        Two icons exist. Desktop and mobile.
        -----------------------*/

        function updateCartIconCount(count) {

            const cartCountContainer = document.querySelectorAll('.cart_count_container');

            const cartCountHTML = `<span class="cart_count">${count}</span>`;

            Array.from(cartCountContainer).forEach((cartCount) => {
                if(count === 0) {
                    cartCount.innerHTML = '';
                }
                else {
                    cartCount.innerHTML = cartCountHTML;
                }
            });

        }

        /*-----------------------
        Remove Item From Cart
        -----------------------*/

        function removeCartItem(lineItem) {

            return new Promise(function (resolve, reject) {
                
                // Check if the user confirms that they want to remove this item.
                // If so then process the removal and resolve the promise.
                // The function 'then' handler will refresh the cart page.

                if (confirm('Remove item from your cart?')) {
                    $.post('/cart/change.js', {
                        line: lineItem,
                        quantity: 0
                    }, function (updatedCartData) {
                        resolve();
                    }, 'json');
                }
                else {
                    reject();
                }

            });

        }

        /*-----------------------
        Change Cart Item
        ---
        Changes a specific items quantity in the cart.
        -----------------------*/

        function changeCartItem(variantId, quantity, subscriptionProperties = null) {

            return new Promise(function (resolve, reject) {
            
                $.getJSON('/cart.js', function (cartData) {
                    
                    // Save all of the items in the cart

                    const cartItems = cartData.items;

                    // Find our specific item by matching the variant we are updating and properties object.
                    // Because there are subscription products, multiple items in the cart may have the same variant id.
                    // Therefore we also need to compare the properties of the item. Properties will be null for one time purchase items.

                    const currentItem = cartItems.find(function(item){
                        return item.variant_id == variantId && _.isEqual(item.properties, subscriptionProperties);
                    });

                    // Get the line number of the item in the cart. Need to add 1 to indexOf because Shopify line starts at a 1 based index.

                    const itemLineNumber = cartItems.indexOf(currentItem) + 1;

                    // Save the current quantity of our item

                    const currentItemQuantity = currentItem.quantity;

                    // Calculate the new quantity

                    const newItemQuantity = currentItemQuantity + quantity;

                    // Send the updated quantity to the cart
                    
                    $.post('/cart/change.js', {
                        line: itemLineNumber,
                        quantity: newItemQuantity
                    }, function (updatedCartData) {
                        updateCartIconCount(updatedCartData.item_count);
                        resolve(updatedCartData);
                    }, 'json');

                });

            });

        }

        /*-----------------------
        Add To Cart
        ---
        Takes item data as a parameter and adds it to a users cart. The item data includes the variant, quantity and additional item properties that make it unique.
        -----------------------*/

        function addToCart(itemData) {

            return new Promise(function(resolve, reject){

                // Add the product to the cart

                $.post('/cart/add.js', itemData, function (data) {

                    $.getJSON('/cart.js', function(cartdata) {

                        updateCartIconCount(cartdata.item_count);

                        resolve(cartdata);

                    });

                }, 'json').fail(function (response) {
                    
                    // Could not add product to cart. Likely because no more product in stock.

                    const errorDescription = JSON.parse(response.responseText).description;

                    reject(errorDescription);

                });

            })

        }

        /*-----------------------
        Add To Cart Form Button Click
        ---
        Finds add to cart buttons on a page and triggers an item to add to the cart.
        Also hides itself and shows the quantity selectors.
        -----------------------*/

        const addToCartButtons = document.querySelectorAll('.add_to_cart_button');

        Array.from(addToCartButtons).forEach(addToCartButton => {

            addToCartButton.addEventListener('click', (e) => {

                e.preventDefault();

                // Elements

                const addToCartForm = addToCartButton.closest('form.add_to_cart_form');

                const errorFeedbackMessageElement = addToCartForm.querySelector('.error_feedback_message');

                const checkoutBar = document.querySelector('.checkout_bar');
                
                const productInfo = addToCartForm.querySelector('.add_to_cart_form_product_info');

                // Subscription Info

                const subscriptionId = addToCartForm.querySelector('.rc_subscription_id').value;
                
                const productName = productInfo.dataset.productName;

                const pageName = getPageName();
                
                // Add the variant to the users cart

                const shippingIntervalUnitType = addToCartForm.querySelector('.rc_shipping_interval_unit_type').value;

                const purchaseType = addToCartForm.querySelector('input.rc_radio:checked').value;

                const shippingIntervalFrequency = addToCartForm.querySelector('.rc_shipping_interval_frequency').value;
                
                // Selected Variant ID

                let variantId;
                
                if (purchaseType === 'autodeliver') {
                    variantId = addToCartForm.querySelector('.rc_duplicate_selector').value;
                }
                else {
                    variantId = addToCartForm.querySelector('.variant_selector').value;
                }

                // Add the variant to the users cart. Conditionally set the item data in case the user is subscribing to the product.

                let itemData;

                if(purchaseType === 'onetime') {
                    itemData = {
                        "quantity": 1,
                        "id": variantId
                    }
                }
                else if(purchaseType === 'autodeliver') {
                    itemData = {
                        "quantity": 1,
                        "id": variantId,
                        "properties[shipping_interval_frequency]": shippingIntervalFrequency,
                        "properties[shipping_interval_unit_type]": shippingIntervalUnitType,
                        "properties[subscription_id]": subscriptionId
                    }
                }

                addToCart(itemData).then(() => {

                    // Hide this button & Show the quantity selector

                    addToCartForm.classList.add('show_quantity_updater');

                    // Show the fixed checkout bar

                    checkoutBar.classList.add('visible');

                    // Send add to cart event to DCM and FB

                    trackAddToCart(variantId, purchaseType);

                    // Send to google analytics

                    if(purchaseType === 'onetime') {
                        ga('send', 'event', 'Cart', `Add To Cart - One Time - ${pageName}`, productName);
                    }
                    else if(purchaseType === 'autodeliver') {
                        ga('send', 'event', 'Cart', `Add To Cart - Subscription - ${pageName}`, productName);
                    }
                                        
                }).catch((error) => {
                    errorFeedbackMessageElement.innerHTML = error;
                    setTimeout(() => {
                        errorFeedbackMessageElement.innerHTML = '';
                    }, 5000);
                });

            })

        });

        /*------------------------
        Product Quantity Updater - Add To Cart Form
        ---
        Quantity increase/decrease buttons. Specific functionality for quantity updaters within add to cart forms.
        -------------------------*/

        const addToCartFormQuantityUpdaters = document.querySelectorAll('.add_to_cart_form .quantity_updater');

        Array.from(addToCartFormQuantityUpdaters).forEach(updater => {

            // Setup the initial active click

            let cartUpdaterActiveClick = false;

            updater.addEventListener('click', () => {

                // If still processing the last click then return.
                if (cartUpdaterActiveClick) {
                    return;
                }
                // Else set the active click to true so no other click actions can process until this one is done.
                else {
                    cartUpdaterActiveClick = true;
                }

                /*------------------------
                Variables
                -------------------------*/

                // Elements

                const addToCartForm = updater.closest('form.add_to_cart_form');
                
                const productInfo = addToCartForm.querySelector('.add_to_cart_form_product_info');

                const quantityContainer = updater.closest('.quantity_updater_container');

                const errorFeedbackMessageElement = addToCartForm.querySelector('.error_feedback_message');

                const quantityElement = quantityContainer.querySelector('.quantity');

                const quantityDecreaseElement = quantityContainer.querySelector('.quantity_updater[data-type="decrease"]');

                // Subscription Info

                const subscriptionId = addToCartForm.querySelector('.rc_subscription_id').value;

                const shippingIntervalUnitType = addToCartForm.querySelector('.rc_shipping_interval_unit_type').value;

                const purchaseType = addToCartForm.querySelector('input.rc_radio:checked').value;

                const shippingIntervalFrequency = addToCartForm.querySelector('.rc_shipping_interval_frequency').value;
                
                const productName = productInfo.dataset.productName;

                const pageName = getPageName();

                // Updater Type (Increase or Decrease)

                const updaterType = updater.dataset.type;

                // Current Quantity

                let quantity = Number(quantityElement.innerHTML);

                // Selected Variant ID

                let variantId;

                if (purchaseType === 'autodeliver') {
                    variantId = addToCartForm.querySelector('.rc_duplicate_selector').value;
                }
                else {
                    variantId = addToCartForm.querySelector('.variant_selector').value;
                }

                /*------------------------
                Helper Functions
                -------------------------*/

                // Visually Show An Updated Quantity on this particular quantity updater

                function showUpdatedQuantity(quantity) {
                    
                    return new Promise(function (resolve, reject) {

                        // Set the quantity element to reflect the new quantity

                        quantityElement.innerHTML = quantity;

                        // Set the feedback element to reflect the new quantity

                        const quantityFeedbackElement = addToCartForm.querySelector('.feedback_quantity');

                        quantityFeedbackElement.innerHTML = quantity;

                        resolve();

                    });

                }

                // Change the decrease icon based on the new quantity of a given item.

                function changeDecreaseIcon(newQuantity) {

                    return new Promise(function (resolve, reject) {

                        // If new quantity is 2 then icon should change from trash to minus

                        if (newQuantity === 2) {
                            quantityDecreaseElement.innerHTML = '<i class="fas fa-minus"></i>';
                        }

                        // If new quantity is 1 then the icon should be a trash indicating the user can remove the item from their cart.

                        else if (newQuantity === 1) {
                            quantityDecreaseElement.innerHTML = '<i class="far fa-trash-alt"></i>';
                        }

                        resolve();

                    });

                }

                /*------------------------
                Update Actions
                Check the updater type and perform the appropriate updates
                -------------------------*/

                // Add the variant to the users cart. Conditionally set the item data in case the user is subscribing to the product.

                let itemData;

                let subscriptionProperties;

                if (purchaseType === 'onetime') {
                    itemData = {
                        "quantity": 1,
                        "id": variantId
                    }
                }
                else if (purchaseType === 'autodeliver') {

                    // Item data to send to addToCart()

                    itemData = {
                        "quantity": 1,
                        "id": variantId,
                        "properties[shipping_interval_frequency]": shippingIntervalFrequency,
                        "properties[shipping_interval_unit_type]": shippingIntervalUnitType,
                        "properties[subscription_id]": subscriptionId
                    }

                    // Subscription Properties to be able to find this exact product in the cart items changeCartItem()

                    subscriptionProperties = {
                        shipping_interval_frequency: shippingIntervalFrequency,
                        shipping_interval_unit_type: shippingIntervalUnitType,
                        subscription_id: subscriptionId
                    }
                }

                if (updaterType === 'increase') {

                    const newQuantity = quantity + 1;

                    addToCart(itemData).then(() => {

                        // On successful add to cart show the updated quantity

                        Promise.all([
                            showUpdatedQuantity(newQuantity),
                            changeDecreaseIcon(newQuantity)
                        ]).then(() => {

                            // Send add to cart event to google tag manager

                            dataLayer.push({
                                'event': 'increase-quantity',
                                'pageName': pageName,
                                'productName': productName
                            });

                            if(purchaseType === 'onetime') {
                                ga('send', 'event', 'Cart', `Increase Quantity - One Time - ${pageName}`, productName);
                            }
                            else if(purchaseType === 'autodeliver') {
                                ga('send', 'event', 'Cart', `Increase Quantity - Subscription - ${pageName}`, productName);
                            }

                            // Send add to cart event to DCM and FB

                            trackAddToCart(variantId, purchaseType);

                            // Reset the active click

                            cartUpdaterActiveClick = false;

                        });

                    }).catch((error) => {

                        errorFeedbackMessageElement.innerHTML = error;

                        setTimeout(() => {
                            errorFeedbackMessageElement.innerHTML = '';
                        }, 5000);

                        // Reset the active click

                        cartUpdaterActiveClick = false;

                    });

                }
                else if (updaterType === 'decrease') {

                    const newQuantity = quantity - 1;

                    // Remove one of the item from the cart

                    changeCartItem(variantId, -1, subscriptionProperties).then((cartData) => {
                        
                        if (newQuantity > 0) {

                            Promise.all([
                                showUpdatedQuantity(newQuantity),
                                changeDecreaseIcon(newQuantity)
                            ]).then(() => {
                            
                                // Send decrease quantity event to google tag manager

                                dataLayer.push({
                                    'event': 'decrease-quantity',
                                    'pageName': pageName,
                                    'productName': productName
                                });

                                ga('send', 'event', 'Cart', 'Decrease Quantity', productName);
                                
                                if(purchaseType === 'onetime') {
                                    ga('send', 'event', 'Cart', `Decrease Quantity - One Time - ${pageName}`, productName);
                                }
                                else if(purchaseType === 'autodeliver') {
                                    ga('send', 'event', 'Cart', `Decrease Quantity - Subscription - ${pageName}`, productName);
                                }
                                
                                // Reset the active click

                                cartUpdaterActiveClick = false;

                            });

                        }
                        // If new quantity is 0 then hide the quantity updaters and show the add to cart button.
                        else if(newQuantity === 0) {

                            addToCartForm.classList.remove('show_quantity_updater');

                            // Send remove from cart event to google tag manager
                            dataLayer.push({
                                'event': 'remove-from-cart',
                                'pageName': pageName,
                                'productName': productName
                            });
                            
                            if(purchaseType === 'onetime') {
                                ga('send', 'event', 'Cart', `Remove From Cart - One Time - ${pageName}`, productName);
                            }
                            else if(purchaseType === 'autodeliver') {
                                ga('send', 'event', 'Cart', `Remove From Cart - Subscription - ${pageName}`, productName);
                            }

                            // Reset the active click

                            cartUpdaterActiveClick = false;

                        }

                    });

                }

            })

        });

        /*------------------------
        Product Quantity Updater - Cart Page
        ---
        Quantity increase/decrease buttons. Specific functionality for quantity updaters within the cart page.
        -------------------------*/

        const cartPageQuantityUpdaters = document.querySelectorAll('.cart_page_form .quantity_updater');

        Array.from(cartPageQuantityUpdaters).forEach(updater => {

            // Setup the initial active click

            let cartUpdaterActiveClick = false;

            updater.addEventListener('click', () => {

                // If still processing the last click then return.
                if (cartUpdaterActiveClick) {
                    return;
                }
                // Else set the active click to true so no other click actions can process until this one is done.
                else {
                    cartUpdaterActiveClick = true;
                }

                /*------------------------
                Variables
                -------------------------*/

                // Elements

                const quantityContainer = updater.closest('.quantity_updater_container');

                const quantityElement = quantityContainer.querySelector('.quantity');

                const quantityDecreaseElement = quantityContainer.querySelector('.quantity_updater[data-type="decrease"]');

                const cartItemElement = updater.closest('.cart_item');

                const cartItemTotalElement = cartItemElement.querySelector('.cart_item_total');

                const cartPageForm = updater.closest('.cart_page_form');

                const cartTotalElement = cartPageForm.querySelector('.cart_total');

                const errorFeedbackMessageElement = cartItemElement.querySelector('.error_feedback_message');

                // Subscription Info

                const subscriptionId = quantityContainer.dataset.subscriptionId;

                const shippingIntervalUnitType = quantityContainer.dataset.shippingIntervalUnitType;

                const shippingIntervalFrequency = quantityContainer.dataset.shippingIntervalFrequency;
                
                const productName = cartItemElement.dataset.productName;

                // Updater Type (Increase or Decrease)

                const updaterType = updater.dataset.type;

                // Current Quantity

                let quantity = Number(quantityElement.innerHTML);

                // Variant Info

                const variantId = Number(quantityContainer.dataset.variantId);

                const variantPrice = quantityContainer.dataset.variantPrice;

                const lineItem = Number(quantityContainer.dataset.lineItem);

                /*------------------------
                Helper Functions
                -------------------------*/

                // Set the quantity counter element to reflect the new quantity

                function showUpdatedQuantity(quantity) {

                    return new Promise(function(resolve, reject){
                        quantityElement.innerHTML = quantity;
                        resolve();
                    });

                }

                // Visually indicate the new line item total

                function updateLineItemTotal(itemTotal) {

                    return new Promise(function (resolve, reject) {
                        const updatedLineItemTotal = slate.Currency.formatMoney(itemTotal, '${{ amount }}');
                        cartItemTotalElement.innerHTML = updatedLineItemTotal;
                        resolve();
                    });

                }

                // Visually indicate the new cart total

                function updateCartTotal(cartTotal) {

                    return new Promise(function (resolve, reject) {
                        const updatedCartTotal = slate.Currency.formatMoney(cartTotal, '${{ amount }}');
                        cartTotalElement.innerHTML = updatedCartTotal;
                        resolve();
                    });

                }

                // Change the decrease icon based on the new quantity of a given item.

                function changeDecreaseIcon(newQuantity) {

                    return new Promise(function (resolve, reject) {

                        // If new quantity is 2 then icon should change from trash to minus

                        if (newQuantity === 2) {
                            quantityDecreaseElement.innerHTML = '<i class="fas fa-minus"></i>';
                        }

                        // If new quantity is 1 then the icon should be a trash indicating the user can remove the item from their cart.

                        else if (newQuantity === 1) {
                            quantityDecreaseElement.innerHTML = '<i class="far fa-trash-alt"></i>';
                        }

                        resolve();

                    });

                }

                /*------------------------
                Update Actions
                Check the updater type and perform the appropriate updates
                -------------------------*/

                // Add the variant to the users cart. Conditionally set the item data in case the user is subscribing to the product.

                let itemData;

                let subscriptionProperties;

                if (subscriptionId) {

                    // Item data to send to addToCart()

                    itemData = {
                        "quantity": 1,
                        "id": variantId,
                        "properties[shipping_interval_frequency]": shippingIntervalFrequency,
                        "properties[shipping_interval_unit_type]": shippingIntervalUnitType,
                        "properties[subscription_id]": subscriptionId
                    }

                    // Subscription Properties to be able to find this exact product in the cart items changeCartItem()

                    subscriptionProperties = {
                        shipping_interval_frequency: shippingIntervalFrequency,
                        shipping_interval_unit_type: shippingIntervalUnitType,
                        subscription_id: subscriptionId
                    }
                }
                else {
                    itemData = {
                        "quantity": 1,
                        "id": variantId
                    }
                }

                if (updaterType === 'increase') {

                    addToCart(itemData).then((cartData) => {

                        // On successful add to cart ...

                        quantity += 1;

                        Promise.all([
                            showUpdatedQuantity(quantity),
                            updateLineItemTotal(variantPrice * quantity),
                            updateCartTotal(cartData.total_price),
                            changeDecreaseIcon(quantity)
                        ]).then(() => {
                            
                            // Send add to cart event to google tag manager

                            dataLayer.push({
                                'event': 'increase-quantity',
                                'pageName': 'cart',
                                'productName': productName
                            });

                            if(subscriptionId) {
                                ga('send', 'event', 'Cart', 'Increase Quantity - Subscription - Cart Page', productName);
                            }
                            else {
                                ga('send', 'event', 'Cart', 'Increase Quantity - One Time - Cart Page', productName);
                            }

                            // Send add to cart event to DCM and FB

                            let purchaseType = subscriptionId ? 'autodeliver' : 'onetime';

                            trackAddToCart(variantId, purchaseType);

                            // Reset the active click

                            cartUpdaterActiveClick = false;

                        });

                    }).catch((error) => {

                        // If error then show the error message to the user.

                        errorFeedbackMessageElement.innerHTML = error;
                        setTimeout(() => {
                            errorFeedbackMessageElement.innerHTML = '';
                        }, 5000);

                        // Reset the active click

                        cartUpdaterActiveClick = false;

                    });

                }
                else if (updaterType === 'decrease') {

                    // If current quantity is greater than 1 then allow the user to decrease the amount

                    if (quantity > 1) {
                        
                        changeCartItem(variantId, -1, subscriptionProperties).then((cartData) => {
                            
                            quantity -= 1;

                            Promise.all([
                                showUpdatedQuantity(quantity),
                                updateLineItemTotal(variantPrice * quantity),
                                updateCartTotal(cartData.total_price),
                                changeDecreaseIcon(quantity)
                            ]).then(() => {

                                // Send decrease quantity event to google tag manager

                                dataLayer.push({
                                    'event': 'decrease-quantity',
                                    'pageName': 'cart',
                                    'productName': productName
                                });

                                if(subscriptionId) {
                                    ga('send', 'event', 'Cart', 'Decrease Quantity - Subscription - Cart Page', productName);
                                }
                                else {
                                    ga('send', 'event', 'Cart', 'Decrease Quantity - One Time - Cart Page', productName);
                                }

                                // Reset the active click

                                cartUpdaterActiveClick = false;

                            });

                        });

                    }

                    // Else current quantity is 1 then the user is trying to remove the item.

                    else if(quantity === 1) {
                        removeCartItem(lineItem).then(() => {
                            document.location.href = '/cart';

                            // Send remove from cart event to google tag manager
                            dataLayer.push({
                                'event': 'remove-from-cart',
                                'pageName': 'cart',
                                'productName': productName
                            });

                            if(subscriptionId) {
                                ga('send', 'event', 'Cart', 'Remove From Cart - Subscription - Cart Page', productName);
                            }
                            else {
                                ga('send', 'event', 'Cart', 'Remove From Cart - One Time - Cart Page', productName);
                            }

                        }).catch(() => {
                            // Reset the active click
                            cartUpdaterActiveClick = false;
                            return;
                        });
                    }

                }

            })

        });

        /*------------------------
        Remove Item From Cart - Link Click
        ---
        Removes an item from the users cart.
        -------------------------*/

        const removeCartItemButtons = document.querySelectorAll('.remove_cart_item');

        Array.from(removeCartItemButtons).forEach((button) => {
            
            const lineItem = button.dataset.lineItem;

            const productName = button.dataset.productName;
            
            // Subscription Info

            const subscriptionId = button.dataset.subscriptionId;
            
            button.addEventListener('click', () => {
                
                removeCartItem(lineItem).then(() => {

                    // Send remove from cart event to google tag manager
                    dataLayer.push({
                        'event': 'remove-from-cart',
                        'pageName': 'cart',
                        'productName': productName
                    });
                    
                    if(subscriptionId) {
                        ga('send', 'event', 'Cart', 'Remove From Cart - Subscription - Cart Page', productName);
                    }
                    else {
                        ga('send', 'event', 'Cart', 'Remove From Cart - One Time - Cart Page', productName);
                    }

                    document.location.href = '/cart';

                }).catch(() => {
                    return;
                });
                
            });

        });

        /*-----------------------
        Purchase Type Toggle
        ---
        Toggle between one time and subscription purchase types.
        Activating subscription radio shows the frequency dropdown and extra info popup.
        -----------------------*/

        // Find and loop through all the purchase type radios on the page.

        const rcRadios = document.querySelectorAll('.rc_radio');

        Array.from(rcRadios).forEach(rcRadio => {

            rcRadio.addEventListener('click', () => {

                // Elements

                const addToCartForm = rcRadio.closest('form.add_to_cart_form');

                const oneTimeToggle = addToCartForm.querySelector('.rc_block__type__onetime');

                const subscriptionToggle = addToCartForm.querySelector('.rc_block__type__autodeliver');

                const purchaseType = rcRadio.value;
                
                const productInfo = addToCartForm.querySelector('.add_to_cart_form_product_info');
                
                const productName = productInfo.dataset.productName;
                
                const pageName = getPageName();
                
                const buyNowButton = addToCartForm.querySelector('.shopify-payment-button');
                
                // If one time radio then activate it and hide the subscription info.

                if(purchaseType === 'onetime') {
                    
                    // Hide subscription info
                    
                    oneTimeToggle.classList.add('rc_block__type--active');
                    subscriptionToggle.classList.remove('rc_block__type--active');
                    
                    // Show Buy Now button
                    
                    if(buyNowButton) {
                        buyNowButton.style.display = 'block';
                    }
                    
                    // Send event to GA
                    
                    ga('send', 'event', 'Product', `Toggle One Time - ${pageName}`, productName);
                    
                }

                // Else auto deliver then show subscription info
                
                else if(purchaseType === 'autodeliver') {
                    
                    // Show subscription dropdown
                    
                    oneTimeToggle.classList.remove('rc_block__type--active');
                    subscriptionToggle.classList.add('rc_block__type--active');
                    
                    // Hide Buy Now button
                    
                    if(buyNowButton) {
                        buyNowButton.style.display = 'none';
                    }
                    
                    // Send event to GA
                    
                    ga('send', 'event', 'Product', `Toggle Subscription - ${pageName}`, productName);
                }
                
            });

        });
        
        /*-----------------------
        Buy Now / More Payment Options
        ---
        Buy Now button is created by Shopify. We are just tracking when a user clicks this.
        Using event delegation because the Buy Now button is dynamically added to the code.
        -----------------------*/
                        
        document.querySelector('body').addEventListener("click",function(e) {
            
            // Helper function to track the buy now actions
            
            function trackBuyNow(element, action) {
                
                // Save the product info
                
                const addToCartForm = element.closest('form.add_to_cart_form');
                const productInfo = addToCartForm.querySelector('.add_to_cart_form_product_info');
                const productName = productInfo.dataset.productName;
                const variantId = productInfo.dataset.variantId;
                const buttonType = element.dataset.testid;

                // Track via google analytics
                
                ga('send', 'event', 'Cart', action, productName);

                // Track via DCM

                if (action === 'Buy Now') {

                    gtag('event', 'conversion', {
                        'allow_custom_scripts': true,
                        'u1': getTrackingInfo().googleAnalyticsId + '|' + getTrackingInfo().customerId,
                        'u12': buttonType,
                        'u14': getTrackingInfo().productPrice,
                        'u5': variantId,
                        'u6': '1',
                        'u7': getTrackingInfo().productPrice,
                        'send_to': 'DC-9037312/amazo0/forto0+standard'
                    });

                }
                
            }
            
            if (e.target && e.target.matches(".shopify-payment-button__button")) {
                trackBuyNow(e.target, 'Buy Now');
            }
            
            if (e.target && e.target.matches(".shopify-payment-button__more-options")) {
                trackBuyNow(e.target, 'More payment options');
            }
            
        });
        
        /*-----------------------
        Go To Cart Tracking
        -----------------------*/
        
        function trackGoToCartLink(label) {
            
            const pageName = getPageName();
            
            ga('send', 'event', 'Cart', `Go To Cart - ${pageName}`, label);
            
        }
        
        // Checkout bar.
        
        const checkoutBarButton = document.querySelector('.checkout_bar a');
        
        checkoutBarButton.addEventListener('click', () => {
            
            trackGoToCartLink('Continue To Checkout Bar');
            
        });
        
       // Cart Icon
        
        const cartIconButtons = document.querySelectorAll('li.cart_icon a');
        
        Array.from(cartIconButtons).forEach((cartIconButton) => {
                        
            cartIconButton.addEventListener('click', () => {
                
                const isMobile = cartIconButton.closest('header.mobile_header');
                
                if(isMobile) {
                    trackGoToCartLink('Header Mobile Cart Icon');
                }
                else {
                    trackGoToCartLink('Header Desktop Cart Icon');
                }
                                
            });
        
        });
        
    });
    
})(jQuery);