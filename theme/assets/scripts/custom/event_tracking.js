(function($) {
	$(document).ready(function() {
		'use strict';

		/*----------------------------
		DCM Tracking
		---
		Note: Add To Cart tracking happens directly in cart_ajax.js
		----------------------------*/

		const hasAccount = getTrackingInfo().customerId ? 'true' : 'false';

		// Forto – All Pages – Standard

		gtag('event', 'conversion', {
			allow_custom_scripts: true,
			u1:
				getTrackingInfo().googleAnalyticsId +
				'|' +
				getTrackingInfo().customerId,
			u3: getTrackingInfo().pageUrl,
			u4: getTrackingInfo().productCat,
			u5: getTrackingInfo().productId,
			u17: hasAccount,
			send_to: 'DC-9037312/forto0/forto0+standard',
		});

		if (
			getTrackingInfo().pageName !== 'All Products Page' &&
			getTrackingInfo().pageName !== 'Cart Page'
		) {
			fbq('track', 'ViewContent', {
				custID:
					getTrackingInfo().googleAnalyticsId +
					', ' +
					getTrackingInfo().customerId,
				account: hasAccount,
			});
		}

		// Forto – All Pages – Unique

		gtag('event', 'conversion', {
			allow_custom_scripts: true,
			u1:
				getTrackingInfo().googleAnalyticsId +
				'|' +
				getTrackingInfo().customerId,
			u3: getTrackingInfo().pageUrl,
			u17: hasAccount,
			send_to: 'DC-9037312/forto0/forto00+unique',
		});

		// Forto – Product Pages

		if (getTrackingInfo().productId) {
			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u3: getTrackingInfo().pageUrl,
				u4: getTrackingInfo().productCat,
				u5: getTrackingInfo().productVariantId,
				u7: getTrackingInfo().productPrice,
				u17: hasAccount,
				send_to: 'DC-9037312/forto0/forto001+standard',
			});
		}

		// Forto – Shop

		if (getTrackingInfo().pageName === 'All Products Page') {
			// Shop Page Load Event

			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u3: getTrackingInfo().pageUrl,
				u4: 'Sample Packs',
				u17: hasAccount,
				send_to: 'DC-9037312/forto0/forto000+standard',
			});

			fbq('track', 'ViewContent', {
				custID:
					getTrackingInfo().googleAnalyticsId +
					', ' +
					getTrackingInfo().customerId,
				account: hasAccount,
				content_type: 'product_group',
				contents: 'Sample Packs',
			});

			// Scroll Click on Category Names

			const shopPageCategories = document.querySelectorAll(
				'.product_collections_menu li a'
			);

			Array.from(shopPageCategories).forEach(shopPageCat => {
				shopPageCat.addEventListener('click', function() {
					const productCat = this.innerHTML;

					gtag('event', 'conversion', {
						allow_custom_scripts: true,
						u1:
							getTrackingInfo().googleAnalyticsId +
							'|' +
							getTrackingInfo().customerId,
						u3: getTrackingInfo().pageUrl,
						u4: productCat,
						u17: hasAccount,
						send_to: 'DC-9037312/forto0/forto000+standard',
					});

					fbq('track', 'ViewContent', {
						custID:
							getTrackingInfo().googleAnalyticsId +
							', ' +
							getTrackingInfo().customerId,
						account: hasAccount,
						content_type: 'product_group',
						contents: productCat,
					});
				});
			});
		}

		// Forto – Cart Page

		if (getTrackingInfo().pageName === 'Cart Page') {
			const cartTrackingInfo = document.querySelector('.cart-tracking-info');

			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u3: getTrackingInfo().pageUrl,
				u5: cartTrackingInfo.dataset.productIdsDcm,
				u6: cartTrackingInfo.dataset.productQuantitiesDcm,
				u7: cartTrackingInfo.dataset.productPrices,
				u17: hasAccount,
				send_to: 'DC-9037312/forto00/forto0+standard',
			});

			fbq('track', 'ViewContent', {
				custID:
					getTrackingInfo().googleAnalyticsId +
					', ' +
					getTrackingInfo().customerId,
				content_ids: cartTrackingInfo.dataset.productIdsFb,
				content_type: 'product',
				account: hasAccount,
				quantity: cartTrackingInfo.dataset.productQuantitiesFb,
			});

			fbq('track', 'InitiateCheckout');
		}

		// Comparison Pages - Walmart Specific

		const urlPath = window.location.pathname;

		if (urlPath.includes('drink') && urlPath.includes('wm')) {
			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u17: hasAccount,
				u3: getTrackingInfo().pageUrl,
				send_to: 'DC-9037312/forto0/energ0+standard',
			});
		} else if (urlPath.includes('shot') && urlPath.includes('wm')) {
			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u17: hasAccount,
				u3: getTrackingInfo().pageUrl,
				send_to: 'DC-9037312/forto0/energ00+standard',
			});
		} else if (urlPath.includes('coffee') && urlPath.includes('wm')) {
			gtag('event', 'conversion', {
				allow_custom_scripts: true,
				u1:
					getTrackingInfo().googleAnalyticsId +
					'|' +
					getTrackingInfo().customerId,
				u17: hasAccount,
				u3: getTrackingInfo().pageUrl,
				send_to: 'DC-9037312/forto0/forto002+standard',
			});
		}

		/*----------------------------
		Custom DCM Click Tracking
		---
		DCM tracking parameters added inline to DOM elements to track specific actions.
		----------------------------*/

		const dcmEventLinks = document.querySelectorAll('.dcm-event-tracker');

		Array.from(dcmEventLinks).forEach(link => {
			// Listen to a click event on each event tracker link.

			link.addEventListener('click', () => {
				// Coupon - Links to external coupons.com website.

				if (link.dataset.type === 'coupon') {
					const couponLabel = link.dataset.label;

					gtag('event', 'conversion', {
						allow_custom_scripts: true,
						u1:
							getTrackingInfo().googleAnalyticsId +
							'|' +
							getTrackingInfo().customerId,
						u3: couponLabel,
						u17: hasAccount,
						send_to: 'DC-9037312/forto0/coupon+standard',
					});

					fbq('track', 'Lead', {
						custID:
							getTrackingInfo().googleAnalyticsId +
							', ' +
							getTrackingInfo().customerId,
						contents: couponLabel,
						account: hasAccount,
					});

					// Ad Words - Tagging Report

					const urlPath = window.location.pathname;

					if (urlPath.includes('drink')) {
						gtag_report_conversion(
							window.location.href,
							'AW-835264843/ZBapCMGH9pABEMvCpI4D'
						);
					} else if (urlPath.includes('shot')) {
						gtag_report_conversion(
							window.location.href,
							'AW-835264843/BH0WCMqp6pABEMvCpI4D'
						);
					} else if (urlPath.includes('coffee')) {
						gtag_report_conversion(
							window.location.href,
							'AW-835264843/ucZrCNmB9pABEMvCpI4D'
						);
					} else if (urlPath.includes('coupons')) {
						gtag_report_conversion(
							window.location.href,
							'AW-835264843/nZ9FCI6j9JABEMvCpI4D'
						);
					}

					// Google Analytics

					ga('send', 'event', 'Coupon', 'Get Coupon Button', couponLabel);
				}
			});
		});

		/*------------------
		Conversion Script for checkout. - Shopify
		---
		Can't code this into theme. Has to be coded into the Shopify backend Online Store > Preferences > Google Analytics Scripts.
		---------------------

		ga('require', 'GTM-MT68GKT');

		<!-- Conversion Tracking just for checkout pages. -->

		var pageUrl = window.location.href;

		if (pageUrl.indexOf('checkouts') !== -1 && pageUrl.indexOf('thank_you') === -1) {

			var script = document.createElement('script');
			
			script.onload = function () {

				ga(function (tracker) {
					
					gaClientId = tracker.get('clientId');

					<!-- DCM(Google Double Click) - Global Snippet -->

					window.dataLayer = window.dataLayer || [];
					function gtag() { dataLayer.push(arguments); }
					gtag('js', new Date());
					gtag('config', 'DC-9037312');
					gtag('config', 'AW-835264843');

					<!-- Forto – All Pages – Standard -->

					gtag('event', 'conversion', {
						'allow_custom_scripts': true,
						'u1': gaClientId + '|',
						'u3': window.location.href,
						'send_to': 'DC-9037312/forto0/forto0+standard'
					});

					<!-- Forto – All Pages – Unique -->

					gtag('event', 'conversion', {
						'allow_custom_scripts': true,
						'u1': gaClientId + '|',
						'u3': window.location.href,
						'send_to': 'DC-9037312/forto0/forto00+unique'
					});

					<!-- Forto – Payment method -->

					gtag('event', 'conversion', {
						'allow_custom_scripts': true,
						'u1': gaClientId + '|',
						'u3': window.location.href,
						'send_to': 'DC-9037312/forto00/forto000+standard'
					});

					<!-- FB - Payment Method -->

					!function(f,b,e,v,n,t,s)
					{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
					n.callMethod.apply(n,arguments):n.queue.push(arguments)};
					if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
					n.queue=[];t=b.createElement(e);t.async=!0;
					t.src=v;s=b.getElementsByTagName(e)[0];
					s.parentNode.insertBefore(t,s)}(window, document,'script',
					'https://connect.facebook.net/en_US/fbevents.js');
					fbq('init', '1193407170681353');
					fbq('track', 'PageView');
					fbq('track', 'AddPaymentInfo', {
						custID: gaClientId + '|',
					});

				});
							
			};
			
			script.src = 'https://www.googletagmanager.com/gtag/js?id=DC-9037312';

			document.head.appendChild(script);

		}
		
		/*------------------
		Conversion Script for Sales Confirmation page - Shopify
		---
		Can't code this into theme. Has to be coded into the Shopify backend Settings > Checkout > Additional Scripts.
		Documentation: https://help.shopify.com/en/manual/orders/status-tracking/add-conversion-tracking-to-thank-you-page
		---------------------

		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-91690577-2', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->

		<script async src="https://www.googletagmanager.com/gtag/js?id=DC-9037312"></script>

		<script>

			ga(function (tracker) {

				var gaClientId = tracker.get('clientId');

				<!-- Logs for Testing - Shopify Sales Conversion -->

				console.log('value: {{ order.total_price | money_without_currency }}');
				console.log('Transaction ID: {{ order_number }}');
				console.log('Customer ID: ' + gaClientId + '|{{ customer.id }}');
				console.log('Billing: {{ {{order.billing_address.city}}|{{ order.billing_address.province }}|{{ order.billing_address.zip }}');
				console.log('Shipping: {{ order.shipping_address.city }}|{{ order.shipping_address.province }}|{{ order.shipping_address.zip }}');
				console.log('Payment Method: {% for transaction in transactions %}{{ transaction.gateway }}{% endfor %}');
				console.log('Order ID: {{ order_number }}');
				console.log('Total Cost No Shipping/Tax: {{ order.subtotal_price | money_without_currency }}');
				console.log('Tax/Shipping Cost: {{ order.tax_price | money_without_currency }}|{{ order.shipping_price | money_without_currency }}');
				console.log('One Time/Subscribe: One Time');
				console.log('Product ID per SKU: {% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
				console.log('Quantity per SKU: {% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
				console.log('Original Price per SKU: {% for item in line_items %}{{ item.original_price | money_without_currency }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
				console.log('Promo Price per SKU: {% for item in line_items %}{{ item.price | money_without_currency }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
				console.log('Discount Code: {% for discount_application in order.discount_applications %}{{ discount_application.title }}|{{ discount_application.value }}|{{ discount_application.value_type}}{% if forloop.last == false %}|{% endif %}{% endfor %}');

				<!-- DCM(Google Double Click) - Global Snippet -->

				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());
				gtag('config', 'DC-9037312');
				gtag('config', 'AW-835264843');

				<!-- Forto – All Pages – Standard -->

				gtag('event', 'conversion', {
					'allow_custom_scripts': true,
					'u1': gaClientId + '|{{ customer.id }}',
					'u3': window.location.href,
					'u5': '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'u17': '{% if customer.id != blank %}true{% else %}false{% endif %}',
					'send_to': 'DC-9037312/forto0/forto0+standard'
				});

				<!-- Forto – All Pages – Unique -->

				gtag('event', 'conversion', {
					'allow_custom_scripts': true,
					'u1': gaClientId + '|{{ customer.id }}',
					'u3': window.location.href,
					'u17': '{% if customer.id != blank %}true{% else %}false{% endif %}',
					'send_to': 'DC-9037312/forto0/forto00+unique'
				});

				<!-- DCM Conversion - Shopify Sales Conversion -->

				gtag('event', 'purchase', {
					'allow_custom_scripts': true,
					'value': '{{ order.total_price | money_without_currency }}',
					'transaction_id': '{{ order_number }}',
					'u1': gaClientId + '|{{ customer.id }}',
					'u10': '{{ order.billing_address.city }}|{{ order.billing_address.province }}|{{ order.billing_address.zip }}',
					'u11': '{{ order.shipping_address.city }}|{{ order.shipping_address.province }}|{{ order.shipping_address.zip }}',
					'u12': '{% for transaction in transactions %}{{ transaction.gateway }}{% endfor %}',
					'u13': '{{ order_number }}',
					'u14': '{{ order.subtotal_price | money_without_currency }}',
					'u15': '{{ order.tax_price | money_without_currency }}|{{ order.shipping_price | money_without_currency }}',
					'u16': 'One Time',
					'u17': '{% if customer.id != blank %}true{% else %}false{% endif %}',
					'u5': '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'u6': '{% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'u7': '{% for item in line_items %}{{ item.original_price | money_without_currency }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'u8': '{% for item in line_items %}{{ item.price | money_without_currency }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'u9': '{% for discount_application in order.discount_applications %}{{ discount_application.title }}|{{ discount_application.value }}|{{ discount_application.value_type}}{% if forloop.last == false %}|{% endif %}{% endfor %}',
					'send_to': 'DC-9037312/sales/salescnf+transactions'
				});
				
				<!-- Facebook Conversion - Shopify Sales Conversion -->

				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '1193407170681353');
				fbq('track', 'PageView');
				fbq('track', 'Purchase', {
					'u1': gaClientId + ', {{ customer.id }}',
					content_ids: '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
					quantity: '{% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
					originalprice: '{% for item in line_items %}{{ item.original_price | money_without_currency }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
					promoprice: '{% for item in line_items %}{{ item.price | money_without_currency }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
					discount: '{% for discount_application in order.discount_applications %}{{ discount_application.title }}|{{ discount_application.value }}|{{ discount_application.value_type}}{% if forloop.last == false %}, {% endif %}{% endfor %}',
					paymethod: '{% for transaction in transactions %}{{ transaction.gateway }}{% endfor %}',
					subscribe: 'onetime', 
					ord: '{{ order_number }}',
					value: '{{ order.subtotal_price | money_without_currency }}',
					taxship: '{{ order.tax_price | money_without_currency }}, {{ order.shipping_price | money_without_currency }}',
					currency: 'USD', 
					account: '{% if customer.id != blank %}true{% else %}false{% endif %}',
					bill: '{{ order.billing_address.city }}, {{ order.billing_address.province }}, {{ order.billing_address.zip }}',
					ship: '{{ order.shipping_address.city }}, {{ order.shipping_address.province }}, {{ order.shipping_address.zip }}'
				});

			});

		</script>

		*/

		/*------------------
		Conversion Script for Sales Confirmation page - ReCharge
		---
		Can't code this into theme. Has to be coded into the Shopify backend Apps > Recharge > Checkout Settings > Additional Scripts. Also uses some different liquid variables than Shopify.
		Recharge Specific Documentation: https://support.rechargepayments.com/hc/en-us/articles/360008830713-Variables-for-Email-Notifications-and-Scripts
		---------------------
		
		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-91690577-2', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->

		<script async src="https://www.googletagmanager.com/gtag/js?id=DC-9037312"></script>

		<script type="text/javascript">
		
		ga(function (tracker) {

			var gaClientId = tracker.get('clientId');

			<!-- Logs for Testing - ReCharge Sales Conversion -->

			console.log('value: {{ total_price }}');
			console.log('Transaction ID: {{ id }}');
			console.log('Customer ID: {{ customer.shopify_customer_id }}');
			console.log('Billing: {{ billing_address.city }}|{{ billing_address.province }}|{{ billing_address.zip }}');
			console.log('Shipping: {{ shipping_address.city }}|{{ shipping_address.province }}|{{ shipping_address.zip }}');
			console.log('Payment Method: Credit Card');
			console.log('Order ID: {{ order_number }}');
			console.log('Total Cost No Shipping/Tax: {{ subtotal_price }}');
			console.log('Tax/Shipping Cost: {{ total_tax }}|{{ total_shipping }}');
			console.log('One Time/Subscribe: Subscribe');
			console.log('Product ID per SKU: {% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
			console.log('Quantity per SKU: {% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
			console.log('Original Price per SKU: {% for item in line_items %}{{ item.price }}{% if forloop.last == false %}|{% endif %}{% endfor %}');
			console.log('Promo Price per SKU:');
			console.log('Discount Code: {{ discount_code }}|{{ discount }}');

			<!-- DCM(Google Double Click) - Global Snippet -->

			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'DC-9037312');
			gtag('config', 'AW-835264843');

			<!-- Forto – All Pages – Standard -->

			gtag('event', 'conversion', {
				'allow_custom_scripts': true,
				'u1': gaClientId + '|{{ customer.shopify_customer_id }}',
				'u3': 'window.location.href',
				'u5': '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				'u17': '{% if customer.shopify_customer_id != blank %}true{% else %}false{% endif %}',
				'send_to': 'DC-9037312/forto0/forto0+standard'
			});

			<!-- Forto – All Pages – Unique -->

			gtag('event', 'conversion', {
				'allow_custom_scripts': true,
				'u1': gaClientId + '|{{ customer.shopify_customer_id }}',
				'u3': window.location.href,
				'u17': '{% if customer.shopify_customer_id != blank %}true{% else %}false{% endif %}',
				'send_to': 'DC-9037312/forto0/forto00+unique'
			});

			<!-- DCM Conversion - ReCharge Sales Conversion -->

			gtag('event', 'purchase', {
				'allow_custom_scripts': true,
				'value': '{{ total_price }}',
				'transaction_id': '{{ order_number }}',
				'u1': gaClientId + '|{{ customer.shopify_customer_id }}',
				'u10': '{{ billing_address.city }}|{{ billing_address.province }}|{{ billing_address.zip }}',
				'u11': '{{ shipping_address.city }}|{{ shipping_address.province }}|{{ shipping_address.zip }}',
				'u12': 'Credit Card',
				'u13': '{{ order_number }}',
				'u14': '{{ subtotal_price }}',
				'u15': '{{ total_tax }}|{{ total_shipping }}',
				'u16': 'Subscribe',
				'u17': '{% if customer.shopify_customer_id != blank %}true{% else %}false{% endif %}',
				'u5': '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				'u6': '{% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				'u7': '{% for item in line_items %}{{ item.price }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				'u8': '{% for item in line_items %}{{ item.price }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				'u9': '{{ discount_code }}|{{ discount }}',
				'send_to': 'DC-9037312/sales/salescnf+transactions'
			});

			<!-- Facebook Conversion - ReCharge Sales Conversion -->

			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1193407170681353');
			fbq('track', 'PageView');
			fbq('track', 'Purchase', {
				custID: gaClientId + ', {{ customer.shopify_customer_id }}',
				content_ids: '{% for item in line_items %}{{ item.variant_id }}{% if forloop.last == false %}|{% endif %}{% endfor %}',
				quantity: '{% for item in line_items %}{{ item.quantity }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
				originalprice: '{% for item in line_items %}{{ item.price | money_without_currency }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
				promoprice: '{% for item in line_items %}{{ item.price | money_without_currency }}{% if forloop.last == false %}, {% endif %}{% endfor %}',
				discount: '{{ discount_code }}, {{ discount }}',
				paymethod: 'Credit Card',
				subscribe: 'Subscribe',
				ord: '{{ order_number }}',
				value: '{{ subtotal_price }}',
				taxship: '{{ total_tax }}, {{ total_shipping }}',
				currency: 'USD',
				account: '{% if customer.shopify_customer_id != blank %}true{% else %}false{% endif %}',
				bill: '{{ billing_address.city }}, {{ billing_address.province }}, {{ billing_address.zip }}',
				ship: '{{ shipping_address.city }}, {{ shipping_address.province }}, {{ shipping_address.zip }}'
			});

		});

		</script>

		*/

		/*----------------------------
		Custom GA Tracking
		---
		GA tracking parameters added inline to DOM elements to track specific actions.
		----------------------------*/

		const eventLinks = document.querySelectorAll('.ga_event_tracker');

		Array.from(eventLinks).forEach(link => {
			// Listen to a click event on each event tracker link.

			link.addEventListener('click', () => {
				const category = link.dataset.gaCategory;
				const action = link.dataset.gaAction;
				const label = link.dataset.gaLabel;

				ga('send', 'event', category, action, label);
			});
		});
	});
})(jQuery);
