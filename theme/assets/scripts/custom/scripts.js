(function($) {

	$(document).ready(function() {

		"use strict";

		/*----------------------------
		Animate Page Content
		----------------------------*/

		const mainContent = document.querySelector('.main_content');
		
		window.setTimeout(function () {
			mainContent.classList.add('loaded');
		}, 500);

		/*----------------------------
		SMOOTH SCROLL
		----------------------------*/

		$('.smooth_scroll').click(function () {

			// Breakpoint to adjust the offset distance.

			const tabletLandscapeBreakpoint = window.matchMedia("(max-width: 1024px)");

			// Get the target element & set normal offset

			let target = $(this).attr('data-target');
			let offsetDistance = -120;

			// If below our breakpoint then adjust offset for a smaller mobile header

			if (tabletLandscapeBreakpoint.matches) {
				offsetDistance = -95;
			}

			target = $('#' + target);

			$('html, body').animate({
				scrollTop: target.offset().top + offsetDistance,
			}, 1000);
			
			return false;

		});
		
	});

})(jQuery);