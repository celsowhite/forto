(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
        Klaviyo AJAX
        =================================*/

        // Set up form variables

        var klaviyoForm = $('.klaviyo-form');

        // On submit of the form send an ajax request to Klavyio for data.

        klaviyoForm.submit(function(e){

            // Make sure the form doesn't link anywhere on submit.

            e.preventDefault();

            // Elements

            const formContainer = this.closest('.email-signup-form');

            const responseElement = formContainer.querySelector('.email-signup-form__response');

            // Get this specific forms info

            const listId = this.querySelector('input[name="g"]').value;

            const email = this.querySelector('input[name="email"]').value;

            // Settings for our ajax request to add the user to a list.

            const subscribeSettings = {
                "async": true,
                "crossDomain": true,
                "url": "https://manage.kmail-lists.com/subscriptions/external/subscribe",
                "method": "POST",
                "headers": {
                    "content-type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache"
                },
                "data": {
                    "g": listId,
                    "email": email
                }
            };

            function showResponse(message) {
                responseElement.innerHTML = message;
                responseElement.classList.remove('email-signup-form__response--is-hidden');
                window.setTimeout(function () {
                    responseElement.innerHTML = '';
                    responseElement.classList.add('email-signup-form__response--is-hidden');
                }, 2000);
            }

            function hideResponse() {
                window.setTimeout(function () {
                    responseElement.innerHTML = '';
                    responseElement.classList.add('email-signup-form__response--is-hidden');
                }, 2000);
            }

            $.ajax(subscribeSettings).done(function (response) {
                showResponse('Success');
            }).fail(function (response) {
                // Klavyio returns a 400 error response if any error occurs. Even duplicate email signup errors.
                showResponse('Error');
            });
            
        });

	});

})(jQuery);