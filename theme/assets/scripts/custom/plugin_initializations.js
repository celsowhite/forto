(function($) {

	$(document).ready(function() {

		"use strict";
		
		/*----------------------------
		Mixitup
		----------------------------*/
		
		if($('.mixitup_container').length) {
			
			const mixer = mixitup('.mixitup_container');
			
		}

		/*----------------------------
		FITVIDS
		----------------------------*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();
		
		/*----------------------------
		Drift - Image Zoom
		----------------------------*/
		
		const zoomImages = document.querySelectorAll('img.zoom_image');
		
		Array.from(zoomImages).forEach((zoomImage) => {
			
			const paneContainer = zoomImage.closest('li');
			
			new Drift(zoomImage, {
				paneContainer: paneContainer,
				zoomFactor: 3
			});
		});

	});

})(jQuery);