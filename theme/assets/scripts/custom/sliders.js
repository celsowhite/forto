(function($) {

     $('.flexslider.full_width_slider').flexslider({
         animation: "slide",
         controlNav: false,
         directionNav: true,
         prevText: "<i class='fa fa-angle-left'></i>",
         nextText: "<i class='fa fa-angle-right'></i>",
         smoothHeight: true
     });

	$(document).ready(function() {

        "use strict";
        
        /*----------------------------
		Flexslider
        ----------------------------*/
        
        // Regular Slider

        $('.flexslider.normal_slider').flexslider({
            animation: "fade",
            controlNav: true,
            directionNav: false,
        });

        // Quote Slider

        $('.flexslider.quote_slider').flexslider({
            animation: "slide",
            controlNav: true,
            directionNav: true,
            prevText: "<i class='fa fa-angle-left'></i>",
            nextText: "<i class='fa fa-angle-right'></i>",
            smoothHeight: true
        });
        
        // Product Page Slider

        $('.flexslider.product_slider').flexslider({
            animation: "fade",
            controlNav: true,
            directionNav: false,
            slideshow: false,
            manualControls: 'ul.product_slider_thumbnails li'
        });

        // Tabbed Slider

        $('.tabbed-slider').flexslider({
            animation: "fade",
            controlNav: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 2000,
            touch: false,
            manualControls: 'ul.tabs li'
        });

		/*----------------------------
		Slick Carousel
		----------------------------*/

        // Center Mode

        $('.slick_carousel.center_mode').slick({
            centerMode: true,
            centerPadding: '20px',
            slidesToShow: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>'
        });
        
        // Photo Carousel

        $('.slick_carousel.photo_carousel').slick({
            centerMode: true,
            centerPadding: '20px',
            slidesToShow: 5,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });

        // Testimonial Carousel

        $('.slick_carousel.testimonial-carousel').slick({
            centerMode: true,
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

	});

})(jQuery);