/*-----------------------
Get Tracking Info
---
Called within other scripts to get global tracking parameters.
These are usually combined with other parameters we pass to DCM and facebook.
-----------------------*/

function getTrackingInfo() {

	const body = document.querySelector('body');

	// GA Session ID

	let gaClientId;

	ga(function (tracker) {
		gaClientId = tracker.get('clientId');
	});

	const trackingInfo = {
		googleAnalyticsId: gaClientId,
		pageName: body.dataset.pageName,
		pageUrl: window.location.href,
		productId: body.dataset.productId,
		productVariantId: body.dataset.productVariantId,
		productCat: body.dataset.productCat,
		productPrice: body.dataset.productPrice,
		customerId: body.dataset.customerId
	}

	return trackingInfo;

}

/*-----------------------
gtag - Report Conversion
---
Ad words conversion tracking helper.
-----------------------*/

function gtag_report_conversion(url, id) {
	var callback = function () {
		if (typeof (url) != 'undefined') {
			// window.location = url;
		}
	};
	gtag('event', 'conversion', {
		'send_to': id,
		'event_callback': callback
	});
	return false;
}

/*----------------------------
MEDIA QUERIES
----------------------------*/

// Interactions just for desktop

const mq = window.matchMedia( "(min-width: 1024px)" );

/*----------------------------
DEBOUCE HELPER
----------------------------*/

// Handle taxing js tasks
// https://davidwalsh.name/javascript-debounce-function

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};