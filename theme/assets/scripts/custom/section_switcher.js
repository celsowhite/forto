/*----------------------------
Easily show/hide sections of a page by clicking links.
Specifically used on login and forgot password pages to dynamically show forms.
----------------------------*/
    
(function ($) {

  $(document).ready(function () {

    "use strict";

    /*----------------------------
    Section Switcher
    ----------------------------*/

    /*--- Save the different section switcher links and switchable sections on the page ---*/

    const sectionSwitchers = document.querySelectorAll('.section_switcher');

    const switchableSections = document.querySelectorAll('.switchable_section');
    
    /*--- Show Section ---*/

    function showSection(sectionName) {

      // Hide all switchable sections on the page

      Array.from(switchableSections).forEach((section) => {
        section.style.display = 'none';
      });

      // Show the new section

      const sectionToShow = document.querySelector('#' + sectionName);
      sectionToShow.style.display = 'block';

    }

    /*--- Section link click events ---*/

    Array.from(sectionSwitchers).forEach((sectionSwitcher) => {

      // On click show the respective section.

      sectionSwitcher.addEventListener('click', (e) => {

        // Prevent default

        e.preventDefault();

        // Show Section

        showSection(sectionSwitcher.dataset.sectionToShow);

      });

    });

    /*--- Hash Section Changer ---*/

    function checkHash() {

      const hash = window.location.hash;

      // Allow deep linking to recover password form

      if (hash === '#recover') {
        showSection('recover_password_form');
      }

    }

    checkHash();

    /*----------------------------
    Show Reset Password Success Message
    ----------------------------*/

    function checkResetPasswordSuccess() {

      const formState = document.querySelector('.reset_password_success');

      const resetSuccessMessage = document.querySelector('#reset_success_message');

      // Check if our span element for successfully password reset exists on the page. If so then it was successfully submitted.
      // Show the success message that exists outside of the password reset form.

      if(formState) {
        resetSuccessMessage.style.display = 'block';
      }

    }

    checkResetPasswordSuccess();

  });

})(jQuery);