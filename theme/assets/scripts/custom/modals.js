(function($) {

	$(document).ready(function() {

	"use strict";

        /*================================= 
		Elements
		=================================*/

        const modalTriggers = document.querySelectorAll('.modal-trigger');

        const modalClose = document.querySelectorAll('.modal-close');

        /*================================= 
		Helpers
        =================================*/
        
        // Load Modal Video
        // If opening a video modal then add its video attributes and trigger the video to play.

        function loadModalVideo(modalTrigger, modalContainer) {

            // Elements

            const modalPlyrElement = modalContainer.querySelector('.plyr-video');
            const videoType = modalTrigger.dataset.videoType;
            const videoURL = modalTrigger.dataset.videoUrl;

            // Dynamically set the Plyr DOM attributes

            modalPlyrElement.dataset.plyrProvider = videoType;
            modalPlyrElement.dataset.plyrEmbedId = videoURL;

            // Setup the plyr instance

            const modalPlyrInstance = new Plyr('.video-modal .plyr-video');

            // Listen for when plyr is ready then trigger the instance to play

            modalPlyrInstance.on('ready', function (event) {
                const instance = event.detail.plyr;
                instance.play();
            });

        }

        /*================================= 
		Modal Open
		=================================*/

        document.querySelector("body").addEventListener("click", function (e) {

            // Use event delegation to open a modal.
            // Check if the clicked element is a modal trigger or is a child of a modal trigger.

            const modalTrigger = e.target.closest('.modal-trigger');

            if (modalTrigger) {

                e.preventDefault();

                // Elements

                const modalName = modalTrigger.dataset.modal;
                const modalContainer = document.querySelector('.' + modalName);

                // Open the modal

                modalContainer.classList.add('modal--is-open');

                // Video Modal
                
                if (modalName === 'video-modal') {

                    loadModalVideo(modalTrigger, modalContainer);
                
                }

            }

        });

        /*================================= 
		Modal Close
		=================================*/

        Array.from(modalClose).forEach(closeTrigger => {

            closeTrigger.addEventListener('click', function() {

                // Elements

                const modalContainer = this.closest('.modal');

                // Close the modal
                
                modalContainer.classList.remove('modal--is-open');

                // If closing a video modal then reset the inner DOM contents.

                if(modalContainer.classList.contains('video-modal')) {
                    const modalVideo = modalContainer.querySelector('.modal__video');
                    modalVideo.innerHTML = '<div class="plyr-video" data-plyr-provider="" data-plyr-embed-id=""></div>';
                }

            })

        });

	});

})(jQuery);