/*-------------------------
Account Scripts
-------------------------*/

(function($) {

	$(document).ready(function() {

		"use strict";

		/*-----------
		Dropdown Navigation Open/Close
		-----------*/

		const accountNavButton = document.querySelector('.mobile_account_navigation_button');

		const accountNavButtonArrow = document.querySelector('.mobile_account_navigation_button span');

		const accountNav = document.querySelector('.mobile_account_navigation');

		function toggleAccountNavigation() {
			if(!accountNav.classList.contains('open')) {
				accountNav.classList.add('open');
				accountNavButtonArrow.classList.add('rotated_up');
			}
			else {
				accountNav.classList.remove('open');
				accountNavButtonArrow.classList.remove('rotated_up');
			}	
		}

		if(accountNavButton) {
			accountNavButton.addEventListener('click', toggleAccountNavigation);
		}
		
	});

})(jQuery);