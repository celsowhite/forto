(function($) {

	$(document).ready(function() {

		"use strict";

		/*----------------------------
		Elements
		----------------------------*/

		const header = document.querySelector('.main_header');

		const mobileHeader = document.querySelector('.mobile_header');

		const menuIcon = document.querySelector('.menu_icon');

		/*----------------------------
		Fixed Navigation
		----------------------------*/

		// Set a variable for caching the scrollY in the window so we know if we are scrolling up or down.

		let lastScrollTop = 0;

		function transformHeader() {

			// Set the current scroll top

			const currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

			// If the current scroll is greater than the last then we are scrolling down.
			// Also only trigger the header transformation if scrolled 30 pixels from the top.

			if (currentScrollTop > lastScrollTop && lastScrollTop > 3) {
				header.classList.add('scrolled');
				mobileHeader.classList.add('scrolled');
			}
			else if (currentScrollTop === 0) {
				header.classList.remove('scrolled');
				mobileHeader.classList.remove('scrolled');
			}
			else {
				header.classList.remove('scrolled');
				mobileHeader.classList.remove('scrolled');
			}

			// Cache the last scroll top value

			lastScrollTop = currentScrollTop;

		}

		window.addEventListener('scroll', debounce(transformHeader, 50));

		/*----------------------------
		Mobile Navigation
		----------------------------*/

		menuIcon.addEventListener('click', () => {
			mobileHeader.classList.toggle('mobile_nav_open');
		});

	});

})(jQuery);